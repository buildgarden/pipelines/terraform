# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

stages:
  - test
  - build
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS
      when: never
    - when: always

.terraform-base:
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  cache: {}
  dependencies: []
  variables:
    GIT_DEPTH: "1"
  before_script:
    - terraform -version

.terraform-base-run:
  extends: .terraform-base
  variables:
    TF_STATE_NAME: "$TERRAFORM_STATE_NAME"
    TF_ROOT: "$TERRAFORM_ROOT"
    TF_ADDRESS: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TERRAFORM_STATE_NAME}"
  cache:
    key: "$TERRAFORM_STATE_NAME"
    paths:
      - ${TERRAFORM_ROOT}/.terraform
  before_script:
    - terraform -version
    - echo "Entering path '$TERRAFORM_ROOT' for state '$TERRAFORM_STATE_NAME'"
    - cd "${TERRAFORM_ROOT}"

.terraform-apply:
  extends: .terraform-base-run
  stage: deploy
  resource_group: $TERRAFORM_STATE_NAME
  environment:
    name: "$TERRAFORM_STATE_NAME/apply"

.terraform-manual-apply:
  extends: .terraform-apply
  script:
    - gitlab-terraform apply
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

.terraform-auto-apply:
  extends: .terraform-apply
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
    - gitlab-terraform apply
  artifacts:
    reports:
      terraform: "${TERRAFORM_ROOT}/plan.json"
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.terraform-destroy:
  extends: .terraform-base-run
  stage: deploy
  script:
    - gitlab-terraform destroy
  resource_group: $TERRAFORM_STATE_NAME
  environment:
    name: "$TERRAFORM_STATE_NAME/apply"
    action: stop
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

.terraform-root:
  variables:
    TERRAFORM_ROOT: "${CI_PROJECT_DIR}"

.terraform-env:
  variables:
    TERRAFORM_ROOT: "${CI_PROJECT_DIR}/deploy/${TERRAFORM_STATE_NAME}"

.terraform-env-development:
  extends: .terraform-env
  variables:
    TERRAFORM_STATE_NAME: "development"

.terraform-env-production:
  extends: .terraform-env
  variables:
    TERRAFORM_STATE_NAME: "production"

.terraform-env-staging:
  extends: .terraform-env
  variables:
    TERRAFORM_STATE_NAME: "staging"

.terraform-module-build:
  extends: .terraform-base
  stage: deploy
  variables:
    TERRAFORM_MODULE_DIR: ${CI_PROJECT_DIR}
    TERRAFORM_MODULE_NAME: ${CI_PROJECT_NAME}
    TERRAFORM_MODULE_VERSION: ${CI_COMMIT_TAG}

.terraform-module-release:
  extends: .terraform-base
  stage: test
  variables:
    TERRAFORM_MODULE_VERSION: ${CI_COMMIT_TAG}

.terraform-plan:
  extends: .terraform-base-run
  stage: test
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  resource_group: $TERRAFORM_STATE_NAME
  environment:
    name: "$TERRAFORM_STATE_NAME/plan"
  artifacts:
    paths:
      - "${TERRAFORM_ROOT}/plan.cache"
      - "${TERRAFORM_ROOT}/plan.json"
    reports:
      terraform: "${TERRAFORM_ROOT}/plan.json"

.terraform-trivy:
  extends: .terraform-base-run
  stage: build
  script:
    - >-
      curl -sSL https://github.com/aquasecurity/trivy/releases/download/v0.49.1/trivy_0.49.1_Linux-64bit.tar.gz
      | tar -C /usr/local/bin/ -xzf - trivy
    - trivy config "${TERRAFORM_ROOT}/plan.json"

.terraform-validate:
  extends: .terraform-base-run
  stage: test
  script:
    - gitlab-terraform validate

terraform-development-apply:
  extends: [.terraform-env-development, .terraform-manual-apply]
  dependencies: [terraform-development-plan]

terraform-development-auto-apply:
  extends: [.terraform-env-development, .terraform-auto-apply]

terraform-development-destroy:
  extends: [.terraform-env-development, .terraform-destroy]

terraform-development-plan:
  extends: [.terraform-env-development, .terraform-plan]

terraform-development-trivy:
  extends: [.terraform-env-development, .terraform-trivy]
  dependencies: [terraform-development-plan]

terraform-development-validate:
  extends: [.terraform-env-development, .terraform-validate]

terraform-fmt:
  extends: .terraform-base
  stage: test
  script:
    - terraform fmt -recursive -check -diff

terraform-module-build:
  extends: .terraform-module-build
  stage: deploy
  script:
    - TERRAFORM_MODULE_NAME=$(echo "${TERRAFORM_MODULE_NAME}" | tr " _" -) # module-name must not have spaces or underscores, so translate them to hyphens
    - tar -vczf /tmp/${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz -C ${TERRAFORM_MODULE_DIR} --exclude=./.git .
    - 'curl --fail-with-body --location --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file /tmp/${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz
      ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${TERRAFORM_MODULE_NAME}/${TERRAFORM_MODULE_SYSTEM}/${TERRAFORM_MODULE_VERSION}/file'
  rules:
    - if: $CI_COMMIT_TAG

terraform-module-release:
  extends: .terraform-module-release
  stage: test
  script:
    - echo $CI_COMMIT_TAG

terraform-production-apply:
  extends: [.terraform-env-production, .terraform-manual-apply]
  dependencies: [terraform-production-plan]

terraform-production-auto-apply:
  extends: [.terraform-env-production, .terraform-auto-apply]

terraform-production-destroy:
  extends: [.terraform-env-production, .terraform-destroy]

terraform-production-plan:
  extends: [.terraform-env-production, .terraform-plan]

terraform-production-trivy:
  extends: [.terraform-env-production, .terraform-trivy]
  dependencies: [terraform-production-plan]

terraform-production-validate:
  extends: [.terraform-env-production, .terraform-validate]

terraform-staging-apply:
  extends: [.terraform-env-staging, .terraform-manual-apply]
  dependencies: [terraform-staging-plan]

terraform-staging-auto-apply:
  extends: [.terraform-env-staging, .terraform-auto-apply]

terraform-staging-destroy:
  extends: [.terraform-env-staging, .terraform-destroy]

terraform-staging-plan:
  extends: [.terraform-env-staging, .terraform-plan]

terraform-staging-trivy:
  extends: [.terraform-env-staging, .terraform-trivy]
  dependencies: [terraform-staging-plan]

terraform-staging-validate:
  extends: [.terraform-env-staging, .terraform-validate]

terraform-trivy:
  extends: [.terraform-trivy]
  stage: test
  script:
    - >-
      curl -sSL https://github.com/aquasecurity/trivy/releases/download/v0.49.1/trivy_0.49.1_Linux-64bit.tar.gz
      | tar -C /usr/local/bin/ -xzf - trivy
    - trivy config .

terraform-validate:
  extends: [.terraform-root, .terraform-validate]
