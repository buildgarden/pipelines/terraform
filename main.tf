# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

resource "random_string" "random" {
  length           = 16
  special          = true
  override_special = "/@£$"
}
