# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

variable "super_secret" {
  type      = string
  sensitive = true
}
