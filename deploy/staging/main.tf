# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

module "root" {
  source = "../.."

  super_secret = var.super_secret
}
