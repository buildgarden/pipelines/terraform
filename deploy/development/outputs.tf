# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

output "secret" {
  value     = module.root.secret
  sensitive = true
}
