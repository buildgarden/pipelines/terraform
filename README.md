# terraform pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/terraform?branch=main)](https://gitlab.com/buildgarden/pipelines/terraform/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/terraform)](https://gitlab.com/buildgarden/pipelines/terraform/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Deploy Terraform-managed infrastructure from GitLab.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Usage

### Dual deployment preset

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/terraform
    file:
      - terraform-fmt.yml
      - terraform-staging-auto-apply.yml
      - terraform-staging-trivy.yml
      - terraform-staging-validate.yml
      - terraform-production-apply.yml
      - terraform-production-plan.yml
      - terraform-production-trivy.yml
      - terraform-production-validate.yml
```

### Deploy to all default environments

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/terraform
    file:
      - terraform-development-auto-apply.yml
      - terraform-production-apply.yml
      - terraform-production-plan.yml
      - terraform-staging-auto-apply.yml
```

### Publish a Terraform Module

A terraform module will only get published when a new tag is cut.

Make sure you set the following environment variable in the `.gitlab-ci.yml`
at the root level of this project. Without it, the module will fail to
publish. You can set it anything you would like for example: `google`, `aws`,
`azure` etc. In the following example it is being set to `google`.

```yaml
variables:
  TERRAFORM_MODULE_SYSTEM: google
```

### Run `trivy`

`trivy` does misconfiguration scanning for Terraform. Include the pipeline for
the relevant deployments:

```yaml
include:
  - project: buildgarden/pipelines/terraform
    file:
      - terraform-production-trivy.yml
```

### Deploy to a `production` environment

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/terraform
    file:
      - terraform-production-apply.yml
      - terraform-production-plan.yml
```

### Deploy to a `staging` environment

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/terraform
    file:
      - terraform-staging-auto-apply.yml
```

### Run `terraform fmt`

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/terraform
    file:
      - terraform-fmt.yml
```

### Run `terraform validate`

`terraform validate` must be run against a specific deployment. Include the
pipeline for the relevant deployments:

```yaml
include:
  - project: buildgarden/pipelines/terraform
    file:
      - terraform-production-validate.yml
```

### Run `trivy`

`trivy` does misconfiguration scanning for Terraform. Include the pipeline for
the relevant deployments:

```yaml
include:
  - project: buildgarden/pipelines/terraform
    file:
      - terraform-production-trivy.yml
```

## Configuration

### Multiple environments

When multiple environments are deployed from a project, it will likely be
necessary to use different variable values for different environments.

To add credentials specific to an environment, use one of the following
environment scopes:

- development - `development/*`

- production - `production/*`

- staging - `staging/*`

# Example Module Docs

<!-- prettier-ignore-start -->
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [random_string.random](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_super_secret"></a> [super\_secret](#input\_super\_secret) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_secret"></a> [secret](#output\_secret) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- prettier-ignore-end -->
